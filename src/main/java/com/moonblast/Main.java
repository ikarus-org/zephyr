package com.moonblast;

import com.moonblast.listeners.CommandHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws IOException, LoginException {

        String token = Files.readAllLines(Paths.get("token.txt")).get(0);

        JDA jda = JDABuilder.createDefault(token)
            .addEventListeners(new CommandHandler())
            .setAutoReconnect(true)
            .setActivity(Activity.listening(" the silence"))
            .build();
    }
}
