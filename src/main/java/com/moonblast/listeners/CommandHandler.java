package com.moonblast.listeners;

import com.moonblast.commands.Command;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandHandler extends ListenerAdapter {

    final String prefix = "!";

    private List<Command> commands = new ArrayList<>();

    public CommandHandler() {

        Reflections reflections = new Reflections("com.moonblast");

        Set<Class<? extends Command>> commandClasses = reflections.getSubTypesOf(Command.class);

        commandClasses.forEach(commandClass -> {
            if(Modifier.isAbstract(commandClass.getModifiers())) {
                return;
            }
            try {
                commands.add(commandClass.getDeclaredConstructor().newInstance());
            } catch(InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if(!event.getChannelType().isGuild() || event.getAuthor().isBot() || !event.getMessage().getContentRaw().startsWith(prefix)) {
            return;
        }

        String content = event.getMessage().getContentStripped();
        int firstSpacePos = content.indexOf(" ");
        if(firstSpacePos == -1) {
            firstSpacePos = content.length();
        }
        String commandString = content.substring(prefix.length(), firstSpacePos);
        String contentString = content.substring(firstSpacePos);

        Map<String, String> options = new HashMap<>();

        int firstOptionPos = contentString.indexOf("--");
        if(firstOptionPos != -1) {
            // things like '--page=3' or --name="calendars"
            Pattern optionsPattern = Pattern.compile("(-{1,2})([\\w]+)((=)(\\d|\\w+))+");
            Matcher optionsMatcher = optionsPattern.matcher(contentString);

            while(optionsMatcher.find()) {
                options.put(optionsMatcher.group(2).toLowerCase(), optionsMatcher.group(5).toLowerCase());
            }

            // things like '--short'
            Pattern flagsPattern = Pattern.compile("--(\\w+)(?:$|\\s)");
            Matcher flagsMatcher = flagsPattern.matcher(contentString);

            while(flagsMatcher.find()) {
                options.put(flagsMatcher.group(1).toLowerCase(), "true");
            }

            contentString = contentString.substring(0, firstOptionPos);
        }

        String finalContentString = contentString;
        commands.forEach(command -> {
            if(command.triggeredBy(event.getMessage(), commandString)) {
                command.execute(event.getMessage(), finalContentString, options);
            }
        });
    }
}
