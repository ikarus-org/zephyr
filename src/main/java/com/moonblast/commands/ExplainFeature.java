package com.moonblast.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.EmbedType;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.Map;
import java.util.Optional;

public class ExplainFeature extends FeatureCommandBase {

    public ExplainFeature() {
        super(new String[]{"explain", "whatis", "elaborate"}, "N/A");
    }

    @Override
    public void execute(Message message, String content, Map<String, String> options) {
        Optional<Feature> featureWithName = features.stream()
            .filter(feature -> feature.name.equalsIgnoreCase(content.trim()))
            .findFirst();
        if(!featureWithName.isPresent()) {
            return;
        }

        String description;

        if(options.containsKey("short") && options.get("short").equals("true")) {
            description = featureWithName.get().shortDescription;
        } else {
            description = featureWithName.get().description;
        }

        EmbedBuilder builder = new EmbedBuilder();

        builder.setTitle(content)
            .setDescription(description);

        message.getChannel().sendMessage(builder.build()).submit();
    }
}
