package com.moonblast.commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;

import java.util.Arrays;
import java.util.Optional;

public abstract class RoleCommandBase extends Command {
    protected String[] allowedRoleIds = new String[] {
            // Truth Seeker
            "600866330177962014",
            // Lobbyist
            "610289946217152513",
            // Watcher
            "692747861351334356"
    };

    public RoleCommandBase(String[] commandAliases, String description) {
        super(false, false, commandAliases, description);
    }

    @FunctionalInterface
    protected interface UserRoleChangeFunction {
        AuditableRestAction<Void> apply(Guild guild, Member member, Role role);
    }
}
