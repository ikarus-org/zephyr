package com.moonblast.commands;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Role;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class RemoveRole extends RoleCommandBase {

    public RemoveRole() {
        super(new String[] {"relinquish", "retireas", "stopbeing"}, "N/A");
    }

    @Override
    public void execute(Message message, String content, Map<String, String> options) {
        Member member = message.getMember();

        if(member == null) {
            return;
        }

        Guild guild = message.getGuild();

        String bareName = content.replaceAll(" ", "");

        Optional<Role> selected_role = Optional.empty();

        for(Role role : guild.getRoles()) {
            if (role.getName().replaceAll(" ", "").equalsIgnoreCase(content.replaceAll(" ", ""))){
                selected_role = Optional.of(role);
                break;
            }
        }

        if (selected_role.isPresent()) {
            Role finalSelectedRole = selected_role.get();
            if (Arrays.stream(allowedRoleIds).noneMatch(allowedRole -> allowedRole.equals(finalSelectedRole.getId()))) {
                message.getChannel().sendMessage(
                        "I am not allowed to remove the role \"" + finalSelectedRole.getName() + "\"\n" +
                                "The allowed roles are: [" +
                                Arrays.stream(allowedRoleIds)
                                        .map(id -> Objects.requireNonNull(guild.getRoleById(id)).getName()).collect(Collectors.joining(", ")) +
                                "]").complete();
            } else {
                if (!member.getRoles().contains(finalSelectedRole)) {
                    message.getChannel().sendMessage("You do not have that role").complete();
                } else {
                    guild.removeRoleFromMember(member, finalSelectedRole).complete();

                    message.getChannel().sendMessage("Your role has been removed!").complete();
                }
            }
        } else {
            message.getChannel().sendMessage(
                    "The role \"" + bareName+ "\" doesn't exist on this server").complete();
        }
    }
}
