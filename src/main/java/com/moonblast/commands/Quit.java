package com.moonblast.commands;

import net.dv8tion.jda.api.entities.Message;

import java.util.Map;

public class Quit extends Command {

    public Quit() {
        super(true, false, new String[] {"quit", "exit"}, "N/A");
    }

    @Override
    public void execute(Message message, String content, Map<String, String> options) {
        System.exit(0);
    }
}
