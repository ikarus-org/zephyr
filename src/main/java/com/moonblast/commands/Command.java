package com.moonblast.commands;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;

import java.util.Arrays;
import java.util.Map;

public abstract class Command {

    public boolean ownerOnly;

    public boolean requiresAdminRights;

    public String[] commandAliases;

    public String description;

    public Command(boolean ownerOnly, boolean requiresAdminRights, String[] commandAliases, String description) {
        this.ownerOnly = ownerOnly;
        this.requiresAdminRights = requiresAdminRights;
        this.commandAliases = commandAliases;
        this.description = description;
    }

    public final boolean triggeredBy(Message message, String commandString) {
        boolean ret = true;
        if(ownerOnly && message.getAuthor().getIdLong() != 135818878176460801L) {
            ret = false;
        }
        if(requiresAdminRights && message.getMember() != null &&  !message.getMember().getPermissions().contains(Permission.ADMINISTRATOR)) {
            ret = false;
        }

        if(Arrays.stream(commandAliases).noneMatch(alias -> alias.equals(commandString))) {
            ret = false;
        }

        return ret;
    }

    public abstract void execute(Message message, String content, Map<String, String> options);
}
