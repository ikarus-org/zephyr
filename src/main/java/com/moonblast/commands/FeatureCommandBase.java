package com.moonblast.commands;

import net.dv8tion.jda.api.entities.Message;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class FeatureCommandBase extends Command {

    protected List<Feature> features = Arrays.asList(
        new Feature(
            "Calendars",
            "A system to support dates and events in history",
            "The calendar system will allow users to create and specify their own calendars. \n" +
                "The supported functions will be:\n" +
                "- Custom Weekdays (Monday through Sunday in the gregorian Calendar)\n" +
                "- Custom Months and their lengths (January through December in the gregorian Calendar)\n" +
                "- Custom Eras (BCE and CE in the gregorian Calendar)\n" +
                "- Perhaps leap years with a bit of scripting\n" +
                "\n" +
                "They will then be able to specify date-attributes to delineate specific points of time in that specific calendar."
        ),
        new Feature(
            "Timeline",
            "A system to put dates from the Calendar-System in a context",
            "The timeline system allows users to add stories to specific points in time. It will be connected with the Storyeditor. \n" +
                "You will have a horizontal bar, with specific events. Those events can then be given a Story (see Storyeditor for more information).\n" +
                "Normal dates from attributes will automatically be mapped onto the timeline to allow you to get a feeling for time in your world."
        ),
        new Feature(
            "Inheritance",
            "A system to make blueprints have base-blueprints for common values",
            "The inheritance system allows blueprints to be grouped under other blueprints to share common values. Programmers will already know this functionality under OOP's " +
                "inheritance.\n" +
                "An example for blueprint-inheritance would look like this:\n" +
                "```\n" +
                "Item (Worth, Weight)\n" +
                "- Weapon (Worth - inherited, Weight - inherited, Damage)\n" +
                "- Potion (Worth - inherited, Weight - inherited, Healing Amount)\n" +
                "```\n" +
                "Each Potion and Weapon have a Weight and a Worth, which they inherit from their base class Item.\n" +
                "Every Weapon-Instance will now have a worth attribute, weight attribute, and damage attribute.\n"
        ),
        new Feature(
            "StoryEditor",
            "A system to write stories and snippets and add them to your instances",
            "The StoryEditor is the next big step in Ikarus' development. \n" +
                "It will go beyond normally writing text, since it wonderfully includes your previous creations.\n" +
                "We will support a markdown editor: <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>\n" +
                "where you can add story-snippets to instances. In addition this will be used to support descriptions for blueprints, attributes, constraints, etc. which is " +
                "useful for when you work on projects in a group, come back to a project after some time of not working on it, or share projects with the community.\n" +
                "The special part about the StoryEditor is the way we connect it with the instance system. You could have the following:\n" +
                "```\n" +
                "In [1433] [King Arthur II] invaded the [Kingdom of Aceleria] with his massive army.\n" +
                "```\n" +
                "Everything within `[]` will reference to either a time-point (in this case an entire year) or a previously created Instance, which you can then open to see it's" +
                " specific values.\n" +
                "\n" +
                "In addition, the StoryEditor can be linked with the timeline feature to create and describe events for specific points in time."
        ),
        new Feature(
            "Injectables",
            "A system to have some attributes only exist when a condition is met",
            "Injectables provide an interface to have attributes only be available when a specific condition is met.\n" +
                "Not every character has horns, but if your character's race is \"Succubus\", they probably will. So the attribute \"Horn-Length\" will be bound to the condition," +
                " that the character is a Succubus. The basic idea, similar to how we will treat Interdependables will be to use a node-editor for this (and optionally provide " +
                "javascript-scripting), since it's hard to allow for such complex structures with normal UI schemes."
        ),
        new Feature(
            "Interdependables",
            "A system to make the values or kinds of an attribute depend on conditions",
            "Interdependables are similar to Injectables. \n" +
                "Instead of providing new functionality by a condition, we change the functionality by a condition.\n" +
                "A character's height may depend on its race's height-range. The options for the eye-colour could depend on race's possible eye-colours. \n" +
                "The Weapons a character uses might depend on his Status (noble, peasant, etc.) and so on.\n" +
                "The basic idea here is again, to use a node-editor and optionally provide javascript-scripting to allow users to specify the complex relationships."
        ),
        new Feature(
            "MapEditor",
            "A system to create your own nestable maps and add instances to them",
            "The MapEditor is the biggest step in Ikarus' development. \n" +
                "We're yet to settle on either realistic terrain, or a polygon map. Though presumably it will be the latter, since detail is a luxury that not many people need " +
                "when it comes to the structure of the world (Should you need it, I recommend you check out Genbrush). You can always of course, provide a detailed background for your world, it would only have a tile-pane above it " +
                "(probably hexagonal shaped). \n" +
                "The map editor will be distinct from other map editors by two things:\n" +
                "A: You can create maps within other maps. You may have a world map, and then another map for a country within that world, then another map within that country " +
                "for a city, and then another one for the local chapel within the city. \n" +
                "B: You can reference instances that you created in those maps. Where's that Kingdom oF Aceleria? Ah, right there in that specific region on the map. Where's the" +
                " City of Brassborough? Ah right over here of course, next to the Peleskerian River.\n"
        ),
        new Feature(
            "Plugins",
            "A system to allow users to create plugins for Ikarus using javascript",
            "Plugins are what they seem to be. Basically mods for Ikarus. \n" +
                "The plugin system is pretty much in place already, and we're only fleshing out some more technical issues and connecting everything together. The scripting " +
                "language we chose is javascript, since it's easy to understand, and easy to work with for us, and doesn't require a JVM like Groovy would.\n" +
                "We will release example-plugins and some tutorials for people to get used to them."
        )
    );

    protected HashMap<Integer, String> emojiNamesForNumbers = new HashMap<Integer, String>() {{
        put(0, "zero");
        put(1, "one");
        put(2, "two");
        put(3, "three");
        put(4, "four");
        put(5, "five");
        put(6, "six");
        put(7, "seven");
        put(8, "eight");
        put(9, "nine");
    }};

    protected HashMap<String, Integer> numbersForEmojiNames = new HashMap<String, Integer>() {{
        put("0⃣", 0);
        put("1⃣", 1);
        put("2⃣", 2);
        put("3⃣", 3);
        put("4⃣", 4);
        put("5⃣", 5);
        put("6⃣", 6);
        put("7⃣", 7);
        put("8⃣", 8);
        put("9⃣", 9);
    }};

    public FeatureCommandBase(String[] aliases, String description) {
        super(false, false, aliases, description);
    }

    public abstract void execute(Message message, String content, Map<String, String> options);

    static class Feature {
        String name;

        String shortDescription;

        String description;

        Feature(String name, String shortDescription, String description) {
            this.name = name;
            this.shortDescription = shortDescription;
            this.description = description;
        }
    }
}
