package com.moonblast.commands;

import com.vdurmont.emoji.EmojiManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class ListFeatures extends FeatureCommandBase {

    public ListFeatures() {
        super(new String[] {"listfeatures", "features"}, "N/A");
    }

    @Override
    public void execute(Message message, String content, Map<String, String> options) {

        AtomicInteger limit = new AtomicInteger(10);

        if(options.containsKey("limit")) {
            try {
                limit.set(Integer.parseInt(options.get("limit")));
                if(limit.get() > 10 || limit.get() < 1) {
                    throw new Exception();
                }
            } catch(Exception e) {
                return;
            }
        }

        AtomicInteger page = new AtomicInteger(0);
        if(options.containsKey("page")) {
            try {
                page.set(Integer.parseInt(options.get("page")));
                if(page.get() * limit.get() > features.size() || page.get() < 0) {
                    throw new Exception();
                }
            } catch(Exception e) {
                return;
            }
        }

        MessageChannel channel = message.getChannel();

        EmbedBuilder builder = new EmbedBuilder();

        for(int i = 0; i < Math.min(limit.get(), features.size() - page.get() * limit.get()); i++) {
            Feature feature = features.get(page.get() * limit.get() + i);
            builder.addField(":" + emojiNamesForNumbers.get(i % 10) + ": " + feature.name, feature.shortDescription, false);
        }

        MessageAction sentMessage = channel.sendMessage(builder.build());

        ReactionAddListener listener = new ReactionAddListener(page, limit);

        Message createdMessage = sentMessage.complete();

        for(int i = 0; i < Math.min(limit.get(), features.size() - page.get() * limit.get()); i++) {
            createdMessage.addReaction(
                EmojiManager.getForAlias(emojiNamesForNumbers.get(i%10)).getUnicode()
            ).complete();
        }

        listener.member = message.getMember();
        listener.message = createdMessage;

        createdMessage.getJDA().addEventListener(listener);

        Timer cancelListenerTimer = new Timer(true);

        cancelListenerTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                message.getJDA().removeEventListener(listener);
            }
        }, 60 * 1000);
    }

    class ReactionAddListener extends ListenerAdapter {

        Member member;

        Message message;

        AtomicInteger page;

        AtomicInteger limit;

        ReactionAddListener(AtomicInteger page, AtomicInteger limit) {
            this.page = page;
            this.limit = limit;
        }

        @Override
        public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
            if(event.getMessageIdLong() != message.getIdLong() || event.getMember() != member) {
                return;
            }

            Integer chosenIndex = numbersForEmojiNames.get(event.getReactionEmote().getEmoji());

            Feature feature = features.get(page.get() * limit.get() + chosenIndex);

            EmbedBuilder builder = new EmbedBuilder();
            builder.setTitle(feature.name).setDescription(feature.description);
            message.getChannel().sendMessage(builder.build()).submit();
        }
    }
}
